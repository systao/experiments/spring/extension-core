##### Silly Experiment with Spring FactoryBean to make a cat quack and duck bark using proxy magic.

###### In a nutshell

This mini-project provides an extension mechanism based on annotations. It exposes four annotations:
* \@EnableExtensions
* \@ExtensionPoint
* \@Extension 
* \@Accumulator

Extensions are Spring Components. Annotation @ExtensionPoint should be set on interfaces.

If @EnableExtensions is present we scan the classpath to look for Extension and ExtensionPoint annotated classes. For each Extension, a proxy is created and injected in the context. When a method of the annotated interface is called, all ExtensionPoints that refer the given Extension are invoked and results are aggregated through an accumulator.

Simple, right?  

###### Example

The test folder provides an example of how this can be actionned.

###### What can be this used for?
 
Basically, nothing. You would be much better off with a correct OO approach. This mini-thing was just a one-day fun practice (hence no real UT). If you think this can prove useful just let me know and tell me what is your use case.

###### Alternative

Despite the name, this is not a plugin mechanism:
* no hot-reload (expect through @Refresh if configured)
* no external plugin locations
* no nothing

Plugin scene is scarce in the Spring ecosystem--to say the least. The main reason is the most obvious one: it is not widely needed. Once upon a time the Spring Dynamic Module project aimed to integrate OSGi into Spring. This project has been donated to the Eclipse foundation (Eclipse Gemini). Both Spring Dynamic Module and Eclipse Gemini are now semi-dead or discontinued.
