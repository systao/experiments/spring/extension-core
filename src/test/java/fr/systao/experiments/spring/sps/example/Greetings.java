package fr.systao.experiments.spring.sps.example;

import fr.systao.experiments.spring.sps.annotations.ExtensionPoint;

@ExtensionPoint
interface Greetings {

    String greetings();

    String byebye(String name);
}
