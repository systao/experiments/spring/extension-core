package fr.systao.experiments.spring.sps.example;

import fr.systao.experiments.spring.sps.annotations.Extension;
import org.springframework.beans.factory.annotation.Autowired;

@Extension(extensionPoints = {Greetings.class, Byebyes.class}, order = Integer.MAX_VALUE)
class EnglishGreetings {

    @Autowired
    private Hello hello;

    public String greetings() {
        return hello.hello() + ", ladies and gents";
    }

    public String byebye(String name) {
        return "Bye bye, " + name;
    }

    public int count() {
        return 10;
    }

}
