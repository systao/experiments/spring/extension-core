package fr.systao.experiments.spring.sps.example;

import fr.systao.experiments.spring.sps.context.EnableExtensions;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
class PluginExample {
	
	@Autowired
	private Greetings greetings;
	
	@Autowired
	private Byebyes byebyes;

	@Test
	void test() {
		Assertions.assertThat(greetings.greetings()).isEqualTo("hu.. hello, ladies and gents");
		Assertions.assertThat(greetings.byebye("everyone I greeted")).isEqualTo("Bye bye, everyone I greeted");
		Assertions.assertThat(byebyes.byebye("everyone")).containsExactly("Hasta la vista, baby", "Bye bye, everyone");
		Assertions.assertThat(byebyes.count()).isEqualTo(13);
	}
	
	@Configuration 
	@EnableExtensions(basePackages= {"fr.systao.experiments.spring.sps.example"})
	static class ExampleConfiguration {
		@Bean
		Hello hello() {
			return new Hello();
		}
	}
}



