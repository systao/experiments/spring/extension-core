package fr.systao.experiments.spring.sps.example;

import fr.systao.experiments.spring.sps.annotations.Extension;

@Extension(extensionPoints = {Byebyes.class})
class SpanishGreetings {
    public String byebye(String name) {
        return "Hasta la vista, baby";
    }

    public int count() {
        return 3;
    }
}
