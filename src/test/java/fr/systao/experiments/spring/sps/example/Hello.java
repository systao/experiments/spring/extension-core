package fr.systao.experiments.spring.sps.example;

import org.springframework.stereotype.Component;

@Component
class Hello {
    public String hello() {
        return "hu.. hello";
    }
}
