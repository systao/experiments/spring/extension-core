package fr.systao.experiments.spring.sps.example;

import fr.systao.experiments.spring.sps.accumulator.Add;
import fr.systao.experiments.spring.sps.accumulator.Push;
import fr.systao.experiments.spring.sps.annotations.Accumulator;
import fr.systao.experiments.spring.sps.annotations.ExtensionPoint;

import java.util.List;

@ExtensionPoint
interface Byebyes {

    @Accumulator(Push.class)
    List<String> byebye(String name);

    @Accumulator(Add.class)
    int count();
}
