package fr.systao.experiments.spring.sps.accumulator;

public interface Operation<T, I> {
    T accumulate(I t);
}
