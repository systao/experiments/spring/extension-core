package fr.systao.experiments.spring.sps.accumulator;

public class Noop<E> implements Operation<E, E>  {
    public static final Noop<?> INSTANCE = new Noop<>();

    @Override
    public E accumulate(E e) {
        return e;
    }
}
