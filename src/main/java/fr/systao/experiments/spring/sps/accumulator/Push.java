package fr.systao.experiments.spring.sps.accumulator;

import java.util.ArrayList;
import java.util.Collection;

public class Push<E> implements AggregateOperation<E> {
    private Collection<E> acc = new ArrayList<>();

    @Override
    public Collection<E> accumulate(E t) {
        acc.add(t);
        return acc;
    }
}
