package fr.systao.experiments.spring.sps.accumulator;


import org.apache.commons.lang3.StringUtils;

public class Concat implements Operation<String, String>  {
    private String acc = "";

    @Override
    public String accumulate(String s) {
        if ( s != null ) {
            acc += (StringUtils.isNotBlank(acc) ? "\n" : "") + s.toString();
        }
        return acc;
    }
}
