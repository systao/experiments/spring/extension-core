package fr.systao.experiments.spring.sps.accumulator;

import java.util.Collection;

public interface AggregateOperation<E> extends Operation<Collection<E>, E> {

}
