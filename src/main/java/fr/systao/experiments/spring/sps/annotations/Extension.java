package fr.systao.experiments.spring.sps.annotations;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Extension {
	@AliasFor("extensionPoints")
	Class<?>[] value() default {};

	@AliasFor("value")
	Class<?>[] extensionPoints() default {};

	int order() default 0;
}
