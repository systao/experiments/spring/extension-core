package fr.systao.experiments.spring.sps.context;

import fr.systao.experiments.spring.sps.annotations.Extension;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;

import java.util.Comparator;

@Slf4j
class ExtensionComparator implements Comparator<BeanDefinition> {

    @Override
    public int compare(BeanDefinition definition1, BeanDefinition definition2) {
        try {
            return getOrder(definition2) - getOrder(definition1);
        }
        catch ( Exception e ) {
            log.warn("An error occurred while sorting extensions. Result may be not reproducible.");
            return 0;
        }
    }

    private int getOrder(BeanDefinition definition) throws ClassNotFoundException {
        Class<?> clazz = Class.forName(definition.getBeanClassName());
        AnnotationAttributes annotationAttributes = AnnotatedElementUtils.getMergedAnnotationAttributes(clazz, Extension.class);
        return annotationAttributes.getNumber("order");
    }

}
