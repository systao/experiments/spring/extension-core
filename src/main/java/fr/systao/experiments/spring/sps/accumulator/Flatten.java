package fr.systao.experiments.spring.sps.accumulator;

import java.util.ArrayList;
import java.util.List;

public class Flatten<E> implements Operation<List<E>, List<E>>  {
    private final List<E> acc = new ArrayList<>();

    @Override
    public List<E> accumulate(List<E> e) {
        if ( e != null ) {
            acc.addAll(e);
        }
        return acc;
    }
}
