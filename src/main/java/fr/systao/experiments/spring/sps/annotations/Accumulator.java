package fr.systao.experiments.spring.sps.annotations;

import fr.systao.experiments.spring.sps.accumulator.Noop;
import fr.systao.experiments.spring.sps.accumulator.Operation;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})//, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Accumulator {
    @AliasFor("type")
    Class<? extends Operation> value() default Noop.class;

    @AliasFor("value")
    Class<? extends Operation>  type() default Noop.class;
}
