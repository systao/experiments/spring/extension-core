package fr.systao.experiments.spring.sps.context;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({"rawtypes", "unchecked"})
public class ExtensionProxyFactory<T> {
    private ApplicationContext applicationContext;

    private final Class<T> targetInterface;
    private final Set<Class<?>> implementors;

    @Setter
    private ClassLoader classLoader = ExtensionProxyFactory.class.getClassLoader();

    T createProxy() {
        InvocationHandler handler = new MethodInvocationHandler<T>(implementors, applicationContext);
        return (T) Proxy.newProxyInstance(classLoader, new Class<?>[] { this.targetInterface }, handler);
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
