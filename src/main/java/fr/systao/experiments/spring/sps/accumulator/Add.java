package fr.systao.experiments.spring.sps.accumulator;

public class Add implements Operation<Integer, Integer>  {
    private int acc = 0;

    @Override
    public Integer accumulate(Integer s) {
        return acc += s != null ? s : 0;
    }

}
