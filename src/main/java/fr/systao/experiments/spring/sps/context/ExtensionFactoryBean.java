package fr.systao.experiments.spring.sps.context;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Set;

public class ExtensionFactoryBean implements FactoryBean, ApplicationContextAware {
    private final Class<?> targetInterface;

    private final Set<Class<?>> extensionClasses;

    private final ExtensionProxyFactory<?> factory;

    @Autowired
    public ExtensionFactoryBean(Class<?> targetInterface, Set<Class<?>> extensionClasses) {
        this.targetInterface = targetInterface;
        this.extensionClasses = extensionClasses;
        factory = new ExtensionProxyFactory(targetInterface, extensionClasses);
    }

    @Override
    public Object getObject() throws Exception {
        return factory.createProxy();
    }

    @Override
    public Class<?> getObjectType() {
        return targetInterface;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        factory.setApplicationContext(applicationContext);
    }
}
