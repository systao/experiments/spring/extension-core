package fr.systao.experiments.spring.sps.context;

import fr.systao.experiments.spring.sps.accumulator.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
class MethodInvocationHandler<T> implements InvocationHandler {
    private final Set<Class<?>> implementors;
    private final ApplicationContext applicationContext;

    @Override
    public Object invoke(Object target, Method method, Object[] objects) throws Throwable {
        Operation accumulator = AccumulatorFactory.INSTANCE.getAccumulator(method);

        Object result = null;

        for (Class<?> implementorClass : implementors) {
            Object implementor = applicationContext.getBean(implementorClass);
            Optional<Method> optional = getMethod(implementor, method);
            if (optional.isPresent()) {
                Method implementorMethod = optional.get();
                boolean accessible = implementorMethod.canAccess(implementor);
                try {
                    implementorMethod.trySetAccessible();
                    Object o = implementorMethod.invoke(implementor, objects);
                    result = accumulator.accumulate(o);
                }
                finally {
                    implementorMethod.setAccessible(accessible);
                }
            }
        }

        return result;
    }

    private Optional<Method> getMethod(Object o, Method original) {
        try {
            return Optional.of(o.getClass().getMethod(original.getName(), original.getParameterTypes()));
        }
        catch ( Exception e ) {
            return Optional.empty();
        }
    }
}
