package fr.systao.experiments.spring.sps.context;

import fr.systao.experiments.spring.sps.accumulator.Noop;
import fr.systao.experiments.spring.sps.accumulator.Operation;
import fr.systao.experiments.spring.sps.annotations.Accumulator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class AccumulatorFactory {
    static final AccumulatorFactory INSTANCE = new AccumulatorFactory();

    private Map<Method, Operation> accumulators = new HashMap<>();

    Operation getAccumulator(Method method) throws InstantiationException, IllegalAccessException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
        if ( !accumulators.containsKey(method) ) {
            createAccumulator(method);
        }
        return accumulators.get(method);
    }

    private void createAccumulator(Method method) throws InstantiationException, IllegalAccessException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
        Operation op;
        if ( method.getAnnotation(Accumulator.class) == null ) {
            log.info("Missing @Accumulator annotation on {}.{}. Defaulting to NOOP", method.getDeclaringClass(), method.getName());
            op = Noop.INSTANCE;
        }
        else {
            AnnotationAttributes annotationAttributes = AnnotatedElementUtils.getMergedAnnotationAttributes(method, Accumulator.class);
            Class<? extends Operation> operation = annotationAttributes.getClass("type");
            op = operation.getDeclaredConstructor().newInstance();
        }
        accumulators.put(method, op);
    }
}
