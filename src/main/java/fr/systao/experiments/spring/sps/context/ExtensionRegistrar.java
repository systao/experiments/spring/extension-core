package fr.systao.experiments.spring.sps.context;

import fr.systao.experiments.spring.sps.annotations.Accumulator;
import fr.systao.experiments.spring.sps.annotations.Extension;
import fr.systao.experiments.spring.sps.annotations.ExtensionPoint;
import fr.systao.experiments.spring.sps.accumulator.Operation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.StandardAnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

@Slf4j
public class ExtensionRegistrar implements ImportBeanDefinitionRegistrar, EnvironmentAware {
    private static final BeanNameGenerator BEAN_NAME_GENERATOR = AnnotationBeanNameGenerator.INSTANCE;

    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    @SneakyThrows
    public void registerBeanDefinitions(final AnnotationMetadata metadata, final BeanDefinitionRegistry registry) {

        final AnnotationAttributes annotationAttributes =
                new AnnotationAttributes(metadata.getAnnotationAttributes(EnableExtensions.class.getCanonicalName()));

        final Set<String> basePackages = getBasePackages((StandardAnnotationMetadata) metadata, annotationAttributes);

        Set<BeanDefinition> extensionPoints = new HashSet<>();
        Set<BeanDefinition> extensions = new TreeSet<>(new ExtensionComparator());

        basePackages.forEach(basePackage -> {
            extensionPoints.addAll(getBeanDefinitions(getExtensionPointLookup(), ExtensionPoint.class, basePackage));
            extensions.addAll(getBeanDefinitions(getExtensionLookup(), Extension.class, basePackage));
        });

        //... check multiplicity

        extensions.forEach(extension -> {
            extension.setLazyInit(true);
            registry.registerBeanDefinition(extension.getBeanClassName(), extension);
        });

        registerExtensions(registry, extensionPoints, extensions);
    }

    private void registerExtensions(BeanDefinitionRegistry registry, Set<BeanDefinition> extensionPoints, Set<BeanDefinition> extensions) throws ClassNotFoundException {
        Map<Class<?>, Set<Class<?>>> extensionMap = mapExtensions(extensions);

        for (final BeanDefinition beanDefinition : extensionPoints) {
            Class<?> targetInterface = Class.forName(beanDefinition.getBeanClassName());
            final String beanName = BEAN_NAME_GENERATOR.generateBeanName(beanDefinition, registry);
            if (!registry.containsBeanDefinition(beanName)) {
                registerFactory(targetInterface, extensionMap.get(targetInterface), registry);
            }
        }
    }

    private void registerFactory(Class<?> targetInterface, Set<Class<?>> extensionClasses, BeanDefinitionRegistry registry) {
        String factoryName = getFactoryName(targetInterface, extensionClasses);

        BeanDefinition definition = BeanDefinitionBuilder.rootBeanDefinition(ExtensionFactoryBean.class)
            .addConstructorArgValue(targetInterface)
            .addConstructorArgValue(extensionClasses)
            .setLazyInit(true)
            .getBeanDefinition();

        registry.registerBeanDefinition(factoryName, definition);
    }

    private String getFactoryName(Class<?> targetInterface, Set<Class<?>> extensionClasses) {
        StringBuilder factoryName = new StringBuilder(StringUtils.replace(targetInterface.getName(), ".", "_"));
        for (Class<?> c : extensionClasses) {
            factoryName.append("_").append(StringUtils.replace(c.getName(), ".", "_"));
        }
        factoryName.append("extensionFactory_").append(factoryName.hashCode());
        return factoryName.toString();
    }

    private Map<Class<?>, Set<Class<?>>> mapExtensions(Set<BeanDefinition> extensions) throws ClassNotFoundException {
        Map<Class<?>, Set<Class<?>>> extensionMap = new LinkedHashMap<>();

        for (final BeanDefinition beanDefinition : extensions) {
            Class<?> clazz = Class.forName(beanDefinition.getBeanClassName());

            validateAccumulators(clazz);

            AnnotationAttributes annotationAttributes = AnnotatedElementUtils.getMergedAnnotationAttributes(clazz, Extension.class);
            Class<?>[] declaredExtensionPoints = annotationAttributes.getClassArray("extensionPoints");

            for (Class<?> declaredExtensionPoint : declaredExtensionPoints) {
                if (!extensionMap.containsKey(declaredExtensionPoint)) {
                    extensionMap.put(declaredExtensionPoint, new HashSet<>());
                }
                Set<Class<?>> implementors = extensionMap.get(declaredExtensionPoint);
                implementors.add(clazz);
            }
        }

        return extensionMap;
    }

    @SuppressWarnings("rawtypes")
    private void validateAccumulators(Class<?> clazz) {
        Method[] methods = clazz.getDeclaredMethods();
        //... if accumulators declared check that operation matches return type
        for (Method method : methods) {
            if ( method.getDeclaredAnnotation(Accumulator.class) != null ) {
                AnnotationAttributes annotationAttributes = AnnotatedElementUtils.getMergedAnnotationAttributes(method, Accumulator.class);
                Class<? extends Operation> operation = annotationAttributes.getClass("type");
                log.info("accumulator generic type name = " + operation.getGenericSuperclass().getTypeName());
            }
        }
    }

    private Set<BeanDefinition> getBeanDefinitions(ClassPathScanningCandidateComponentProvider provider,
                                                   Class<? extends Annotation> annotationType,
                                                   String basePackage) {
        provider.resetFilters(false);
        provider.addIncludeFilter(new AnnotationTypeFilter(annotationType));
        return provider.findCandidateComponents(basePackage);
    }

    private ClassPathScanningCandidateComponentProvider getExtensionPointLookup() {
        return new ClassPathScanningCandidateComponentProvider(false, environment) {
                protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
                    AnnotationMetadata metadata = beanDefinition.getMetadata();
                    return metadata.isInterface();
                }
            };
    }

    private ClassPathScanningCandidateComponentProvider getExtensionLookup() {
        return new ClassPathScanningCandidateComponentProvider(false, environment);
    }

    private static Set<String> getBasePackages(final StandardAnnotationMetadata metadata,
                                               final AnnotationAttributes attributes) {
        final String[] basePackages = attributes.getStringArray("basePackages");
        final Set<String> packagesToScan = new LinkedHashSet<>(Arrays.asList(basePackages));

        if (packagesToScan.isEmpty()) {
            // If value attribute is not set, fallback to the package of the annotated class
            return Collections.singleton(metadata.getIntrospectedClass().getPackage().getName());
        }

        return packagesToScan;
    }


}
